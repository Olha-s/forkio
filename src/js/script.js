const menu = document.querySelector('.header__dropdown');
const burger = document.querySelector('.header__burger');


burger.addEventListener('click', function (event) {
    event.stopPropagation();
    this.classList.toggle('active');
    menu.classList.toggle('active');
});

document.addEventListener('click', function (event) {
    const its_menu = event.target === menu || menu.contains(event.target);
    const its_burger = event.target === burger;
    const menu_active = menu.classList.contains('active');

    if(!its_menu && !its_burger && menu_active){
        burger.classList.toggle('active');
        menu.classList.toggle('active');
    }

});